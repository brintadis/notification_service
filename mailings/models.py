import pytz
from django.db import models
from django.utils import timezone
from django.core.validators import RegexValidator


class Mailing(models.Model):
    date_start = models.DateTimeField(verbose_name="Mailing start")
    date_end = models.DateTimeField(verbose_name="Mailing end")
    time_start = models.TimeField(verbose_name="Time to start send message")
    time_end = models.TimeField(verbose_name="Time to end send message")
    message_text = models.TextField(max_length=255, verbose_name="Mailing text")
    tag = models.CharField(max_length=255, verbose_name="Search by tag", blank=True)
    mobile_operator_code = models.CharField(
        verbose_name='Mobile operator code',
        max_length=3,
        blank=True
    )

    @property
    def to_send(self):
        """
        Check if it is available to send and email now.
        Returns:
            _Boolean_: True or False.
        """
        now = timezone.now()
        if self.date_start <= now <= self.date_end:
            return True
        return False

    def __str__(self):
        return f'Mailing {self.id} {self.date_start}'

    class Meta:
        verbose_name = 'Mailing'
        verbose_name_plural = 'Mailings'


class Client(models.Model):
    ALL_TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_validator = RegexValidator(
        regex=r'7^\d{10}$'
    )
    phone_number = models.CharField(
        verbose_name='Phone number',
        max_length=11,
        unique=True,
        validators=[phone_validator]
    )
    mobile_operator_code = models.CharField(
        verbose_name='Mobile operator code',
        max_length=3,
        blank=True
    )
    tag = models.CharField(verbose_name="Search by tag", max_length=255, blank=True)
    client_timezone = models.CharField(
        verbose_name='Time zone',
        choices=ALL_TIMEZONES,
        default='UTC',
        max_length=50
    )

    def save(self, *args, **kwargs):
        self.mobile_operator_code = str(self.phone_number)[1:4]
        return super(Client, self).save(*args, **kwargs)

    def __str__(self) -> str:
        return f'Client {self.id}, number {self.phone_number}'

    class Meta:
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'


class Message(models.Model):
    STATUS_CHOICES = [
        ('sent', 'Sent'),
        ('not sent', 'Not sent'),
    ]

    time_create = models.DateTimeField(verbose_name="Time create", auto_now_add=True)
    sending_status = models.CharField(
        verbose_name="Sending status",
        choices=STATUS_CHOICES,
        max_length=20
    )
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='message')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='message')

    def __str__(self) -> str:
        return f'Message {self.id} for client {self.client}'

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'
