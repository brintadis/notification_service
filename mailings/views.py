from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from .models import Mailing, Client, Message
from .serializers import MailingSerializer, ClientSerializer, MessageSerializer


class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer

    @action(detail=True, methods=['get'])
    def info(self, request, pk=None):
        """Data for a specific mailing list
        """
        queryset_mailing = Mailing.objects.all()
        get_object_or_404(queryset_mailing, pk=pk)
        queryset = Message.objects.filter(mailing_id=pk).all()
        serializer = MessageSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def full_info(self, request):
        """Data for all mailings
        """
        total_mailings = Mailing.objects.count()
        mailing = Mailing.objects.values('id')
        content = {
            'Total number of mailings': total_mailings,
            'The number of messages sent': ''
        }
        mailing_status_info = {}

        for row in mailing:
            result = {'Total messages': 0, 'Sent': 0, 'No sent': 0}
            mails = Message.objects.filter(mailing_pk=row['id']).all()
            sent_mails = mails.filter(sending_status="Sent").count()
            not_sent_mails = mails.filter(sending_status="Not sent").count()

            result['Total messages'] = len(mails)
            result['Sent'] = sent_mails
            result['No sent'] = not_sent_mails
            mailing_status_info[row['id']] = result

        content['The number of messages sent'] = mailing_status_info
        return Response(content)


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
