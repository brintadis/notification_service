# Сервис уведомлений
Стек используемых технологий: DRF 4.0.6, Celery 4.4.7, Flower 0.9.7

## Запуск проекта
Делаем копию проекта
```
https://gitlab.com/brintadis/notification_service.git
```
Создаем и активируем виртуальное окружение
```
python -m venv env
```
```
env\Scripts\activate
```
Создаем файл конфигурации `.env` с необходимыми переменными
```
URL = 'https://probe.fbrq.cloud/v1/send/'
TOKEN = 'YOUR TOKEN'
```
Инициализация базы данных:
```
python manage.py makemigrations
python manage.py migrate
```
Запуск сервера
```
python manage.py runserver
```
Запустить celery
```
celery -A notification_service worker -l info
```
Запустить flower
```
celery -A notification_service flower --port=5555
```
## Запуск проета с помощью Docker
Делаем копию проекта
```
https://github.com/brintadis/notification_service.git
```
Создаем файл конфигурации `.env` с необходимыми переменными
```
URL = 'https://probe.fbrq.cloud/v1/send/'
TOKEN = 'YOUR TOKEN'
```
Запуск контрейнеров
```
docker-compose up -d
```
Остановка контейнеров
```
docker-compose stop
```
# Выполненные дополнительные задания из

`http://0.0.0.0:8000/api/` - api проекта

`http://0.0.0.0:8000/docs/` - docs проекта

`http://0.0.0.0:8000/api/clients/` - клиенты

`http://0.0.0.0:8000/api/mailings/` - рассылки

`http://0.0.0.0:8000/api/mailings/fullinfo/` - общая статистика по всем рассылкам

`http://0.0.0.0:8000/api/mailings/<pk>/info/` - детальная статистика по конкретной рассылке

`http://0.0.0.0:8000/api/messages/` - сообщения

`http://0.0.0.0:5555` - celery flower
